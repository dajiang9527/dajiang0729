package com.dream.bean;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "customer")
public class Custmer implements Serializable {

    private static final long serialVersionUID = 5719853787731424062L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cst_id")
    protected int id;
    @Column(name = "cst_name")
    private String name;
    @Column(name = "cst_industry")
    private String industry;
    @Column(name = "cst_address")
    private String address;
    @Column(name = "cst_level")
    private String level;
    @Column(name = "cst_phone")
    private String phone;
    @Transient
    private String description;
    @Column(name = "hrTime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date hrTime;

    public Custmer() {
    }

    public Custmer(String name, String industry, String address, String level, String phone, Date hrTime) {
        this.name = name;
        this.industry = industry;
        this.address = address;
        this.level = level;
        this.phone = phone;
        this.hrTime = hrTime;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIndustry() {
        return industry;
    }

    public void setIndustry(String industry) {
        this.industry = industry;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getHrTime() {
        return hrTime;
    }

    public void setHrTime(Date hrTime) {
        this.hrTime = hrTime;
    }

    @Override
    public String toString() {
        return "Custmer{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", industry='" + industry + '\'' +
                ", address='" + address + '\'' +
                ", level='" + level + '\'' +
                ", phone='" + phone + '\'' +
                '}';
    }
}
