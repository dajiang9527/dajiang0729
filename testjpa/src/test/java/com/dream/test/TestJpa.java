package com.dream.test;

import com.dream.bean.Custmer;
import org.junit.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class TestJpa {
    @Test
    public void test(){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("myjpa");
        EntityManager em = factory.createEntityManager();
        EntityTransaction ts = em.getTransaction();
        ts.begin();
        try {
            Custmer c = new Custmer("小朱","日本","东京","经理","13390809345",null);
            em.persist(c);
            ts.commit();
        } catch (Exception e) {
            e.printStackTrace();
            ts.rollback();
        } finally {
        }

        if (em!=null){
            em.close();
        }
        if(factory!=null){
            factory.close();
        }

    }
    @Test
    public void test02(){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("myjpa");
        EntityManager em = factory.createEntityManager();
        EntityTransaction ts = em.getTransaction();
        ts.begin();
        try {
            Custmer custmer = em.find(Custmer.class, 1);
            em.remove(custmer);
            ts.commit();
        } catch (Exception e) {
            ts.rollback();
            e.printStackTrace();
        }
        if (em!=null){
            em.close();
        }
        if(factory!=null){
            factory.close();
        }

    }
    @Test
    public void testString(){
        String s1="programming";
        String s2=new String("programming");
        String s3="program";
        String s4="ming";
        String s5=s3+s4;
        String s6="program"+"ming";
        System.out.println(s1==s2);
        System.out.println(s1==s5);
        System.out.println(s1==s6);
    }
}
