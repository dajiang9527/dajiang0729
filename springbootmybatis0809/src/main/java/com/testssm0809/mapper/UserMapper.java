package com.testssm0809.mapper;

import com.testssm0809.bean.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Mapper
public interface UserMapper {

    public User login(Map<String,Object> map);

}
