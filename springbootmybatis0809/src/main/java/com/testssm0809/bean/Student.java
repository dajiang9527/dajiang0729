package com.testssm0809.bean;


public class Student {

  private long sid;
  private String sname;
  private String spass;
  private String sno;
  private long stid;

  private Teacher teacher;

  public Teacher getTeacher() {
    return teacher;
  }

  public void setTeacher(Teacher teacher) {
    this.teacher = teacher;
  }

  public long getSid() {
    return sid;
  }

  public void setSid(long sid) {
    this.sid = sid;
  }


  public String getSname() {
    return sname;
  }

  public void setSname(String sname) {
    this.sname = sname;
  }


  public String getSpass() {
    return spass;
  }

  public void setSpass(String spass) {
    this.spass = spass;
  }


  public String getSno() {
    return sno;
  }

  public void setSno(String sno) {
    this.sno = sno;
  }


  public long getStid() {
    return stid;
  }

  public void setStid(long stid) {
    this.stid = stid;
  }

}
