package com.testssm0809.bean;


public class User {

  private long uid;
  private String uname;
  private String upass;
  private String uno;


  public long getUid() {
    return uid;
  }

  public void setUid(long uid) {
    this.uid = uid;
  }


  public String getUname() {
    return uname;
  }

  public void setUname(String uname) {
    this.uname = uname;
  }


  public String getUpass() {
    return upass;
  }

  public void setUpass(String upass) {
    this.upass = upass;
  }


  public String getUno() {
    return uno;
  }

  public void setUno(String uno) {
    this.uno = uno;
  }

}
