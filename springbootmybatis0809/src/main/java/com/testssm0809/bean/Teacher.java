package com.testssm0809.bean;


import java.util.List;

public class Teacher {

  private long tid;
  private String tname;
  private String tpass;
  private String tno;
  private long tuid;

  private List<Student> students;

  public List<Student> getStudents() {
    return students;
  }

  public void setStudents(List<Student> students) {
    this.students = students;
  }

  public long getTid() {
    return tid;
  }

  public void setTid(long tid) {
    this.tid = tid;
  }


  public String getTname() {
    return tname;
  }

  public void setTname(String tname) {
    this.tname = tname;
  }


  public String getTpass() {
    return tpass;
  }

  public void setTpass(String tpass) {
    this.tpass = tpass;
  }


  public String getTno() {
    return tno;
  }

  public void setTno(String tno) {
    this.tno = tno;
  }


  public long getTuid() {
    return tuid;
  }

  public void setTuid(long tuid) {
    this.tuid = tuid;
  }

}
