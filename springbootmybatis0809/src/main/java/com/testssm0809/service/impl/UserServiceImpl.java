package com.testssm0809.service.impl;

import com.testssm0809.bean.User;
import com.testssm0809.mapper.UserMapper;
import com.testssm0809.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserMapper userMapper;

    @Override
    public int login(Map<String, Object> map) {
        User user = userMapper.login(map);
        if(user!=null){
            return 1;
        }else {
            return 0;
        }
    }
}
