package com.testssm0809.service;

import com.testssm0809.bean.User;

import java.util.Map;

public interface UserService {

    public int login(Map<String,Object> map);
}
